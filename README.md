# Floppy

Blah blah blah

- <https://bitbucket.org/rakkarage/floppy/issues>
- <https://github.com/rakkarage/floppy/issues>

## Submodules

To include submodules, clone with the --recursive flag:

`git clone --recursive https://bitbucket.org/rakkarage/Floppy.git`

or download a zip from bitbucket (bitbucket-pipelines.yml) which includes all submodules.

<https://bitbucket.org/rakkarage/floppy/downloads/>
