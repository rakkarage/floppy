extends Spatial

var target : Spatial
onready var _mesh : MeshInstance = $Mesh

func _process(delta) -> void:
	if not target:
		return
	var from := _mesh.global_transform.origin
	var to := target.global_transform.origin
	var direction := to - from
	var t := _mesh.global_transform.looking_at(to + direction, Vector3.UP)
	_mesh.global_transform.basis = Quat(_mesh.global_transform.basis).slerp(Quat(t.origin), delta)
