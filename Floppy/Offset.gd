extends Button

const _offset := 2
onready var _texture := $Texture

func _ready() -> void:
	assert(connect("button_down", self, "_down") == OK)
	assert(connect("button_up", self, "_up") == OK)

func _down() -> void:
	var p: Vector2 = _texture.rect_position
	_texture.rect_position = Vector2(p.x, p.y + _offset)

func _up() -> void:
	var p: Vector2 = _texture.rect_position
	_texture.rect_position = Vector2(p.x, p.y - _offset)
