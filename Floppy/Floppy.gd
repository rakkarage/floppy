extends Control

onready var _parent := $Parent
onready var _target := $Target
const _flop := preload("res://Floppy/Flop.tscn")
const _offset := 0.64
const _scale := 128.0

func _ready() -> void:
	for i in range(10):
		var instance := _flop.instance()
		instance.translation = Vector3(i * _offset, 0, 0)
		instance.target = _target
		_parent.add_child(instance)

func _input(event: InputEvent) -> void:
	if event is InputEventScreenDrag:
		_parent.translation += Vector3(event.relative.x / _scale, 0, 0)
		_order()

func _order() -> void:
	for spatial in _parent.get_children():
		var x = _parent.translation.x - spatial.translation.x
		spatial.translation = Vector3(spatial.translation.x, 0, 3 - abs(x))

# using System.Linq;
# using UnityEngine;
# using UnityEngine.EventSystems;
# public class Flop : UIBehaviour, IDragHandler
# {
# 	public float Offset = 64f;
# 	public Transform LookAt;
# 	protected override void Start()
# 	{
# 		base.Start();
# 		for (int i = 0; i < transform.childCount; i++)
# 		{
# 			var x = i * Offset;
# 			Drag(x, transform.GetChild(i));
# 		}
# 		Order();
# 	}
# 	public void Drag(PointerEventData e)
# 	{
# 		foreach (Transform i in transform)
# 		{
# 			var x = i.localPosition.x + e.delta.x;
# 			Drag(x, i);
# 		}
# 		Order();
# 	}
# 	private void Order()
# 	{
# 		var children = GetComponentsInChildren<Transform>();
# 		var sorted = from child in children orderby child.localPosition.z descending select child;
# 		for (int i = 0; i < sorted.Count(); i++)
# 		{
# 			sorted.ElementAt(i).SetSiblingIndex(i);
# 		}
# 	}
# 	private void Drag(float x, Transform t)
# 	{
# 		t.localPosition = new Vector3(x, transform.localPosition.y, x < 0 ? -x : x);
# 	}
# 	public void OnDrag(PointerEventData e)
# 	{
# 		Drag(e);
# 	}
# }
