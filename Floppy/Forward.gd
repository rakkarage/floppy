extends Spatial

# forward input to viewport
# https://godotengine.org/asset-library/asset/127

onready var _viewport: Viewport = $Viewport
onready var _size: Vector2 = $Mesh.mesh.size
onready var _area: Area = $Mesh/Area
onready var _camera := get_viewport().get_camera()
const _distance := 8

func _input(event: InputEvent) -> void:
	if not (event is InputEventMouseButton or event is InputEventMouseMotion or event is InputEventScreenDrag or event is InputEventScreenTouch):
		return
	var from := _camera.project_ray_origin(event.position)
	var to := from + _camera.project_ray_normal(event.position) * _distance
	var result := get_world().direct_space_state.intersect_ray(from, to, [], _area.collision_layer, false, true)
	if result.size() > 0:
		var p: Vector3 = _area.global_transform.affine_inverse() * result.position
		event.position = (Vector2(p.x, -p.y) + _size / 2) / _size * _viewport.size
		_viewport.input(event)
