extends MeshInstance

const _f := 0.0333
onready var _v := get_viewport()

func _process(delta) -> void:
	var c := _v.size * 0.5
	var p := _v.get_mouse_position()
	var x := clamp((p.x - c.x) / c.x, -1.0, 1.0)
	var y := clamp((p.y - c.y) / c.y, -1.0, 1.0)
	transform.basis = Quat(transform.basis).slerp(Quat(Vector3(y * _f, x * _f, 0)), delta)
